package com.pajato.edgar

import org.junit.Rule
import org.junit.rules.TestName
import kotlin.test.AfterTest
import kotlin.test.BeforeTest
import kotlin.test.Test
import kotlin.test.assertTrue

class ComponentTest {
    private val subsystem = "Main"

    @get:Rule
    val testName = TestName()

    @BeforeTest
    fun setUp() = showTestMessage("Starting", testName.methodName, subsystem)

    @AfterTest
    fun tearDown() = showTestMessage("Stopping", testName.methodName, subsystem)

    @Test
    fun `When valid arguments are provided, verify end to end behavior!`() {
        main(arrayOf(
                "server:kls:${getLanguageServerCommand()}:.kt:.kts",
                "config:rootPath:${System.getProperty("user.dir")}",
            )
        )
        assertTrue(logger.entries.size > 0)
    }
}
