package com.pajato.edgar


fun getLanguageServerCommand(category: String = "default"): String {
    fun isWindows() = System.getProperty("os.name").toLowerCase().contains("windows")
    fun unsupported() = "Unsupported category: $category"
    val windowsMap = mapOf( // need to implement a test server asap
        "default" to "kotlin-language-server.bat",
        "non-lsp" to "echo.bat xyz",
    )
    val unixMap = mapOf(
        "default" to "kotlin-language-server",
        "non-lsp" to "echo xyz",
    )

    return when {
        isWindows() -> windowsMap[category] ?: unsupported()
        else -> unixMap[category] ?: unsupported()
    }
}
