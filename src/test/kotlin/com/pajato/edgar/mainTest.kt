package com.pajato.edgar

import java.time.Duration
import java.time.Instant.now
import java.time.temporal.ChronoUnit

var startTimeMillis = 0L

fun showTestMessage(phaseName: String, testName: String, subsystem: String) {
    fun start() {
        fun clearTestData() {
            configuration.clear()
        }

        clearTestData()
        startTimeMillis = now().toEpochMilli()
    }

    fun stop(duration: Long) {
        fun printLogEntries() {
            val logEntries = logger.entries
            println("Number of log entries: ${logEntries.size}")
            if (logEntries.isNotEmpty()) logEntries.forEach { println(it) }
        }

        printLogEntries()
        println("Elapsed time for test: ${subsystem}#`$testName`): $duration")
    }

    println("""$phaseName test: $subsystem#`$testName`.""")
    when (phaseName.toLowerCase()) {
        "starting" -> start()
        "stopping" -> stop(Duration.of(now().toEpochMilli() - startTimeMillis, ChronoUnit.MILLIS).toMillis())
        else -> throw java.lang.IllegalArgumentException("Invalid phase name.")
    }
}
