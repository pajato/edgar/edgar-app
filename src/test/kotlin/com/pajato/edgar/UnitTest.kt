package com.pajato.edgar

import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestName
import kotlin.test.assertFailsWith

class UnitTest {
    private val subsystem = "Main"

    @get:Rule
    val testName = TestName()

    @Before
    fun setUp() = showTestMessage("Starting", testName.methodName, subsystem)

    @After
    fun tearDown() = showTestMessage("Stopping", testName.methodName, subsystem)

    @Test
    fun `When no arguments are provided, verify illegal argument exception is thrown!`() {
        assertFailsWith<IllegalArgumentException> { main(arrayOf()) }
    }

    @Test
    fun `When an empty argument is provided, verify illegal state exception is thrown!`() {
        assertFailsWith<IllegalStateException> { main(arrayOf("")) }
    }
}
