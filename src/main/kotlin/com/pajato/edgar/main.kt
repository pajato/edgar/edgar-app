package com.pajato.edgar

import com.pajato.edgar.collector.DataCollector
import com.pajato.edgar.core.Settings
import com.pajato.edgar.instrumenter.ReporterGenerator
import com.pajato.edgar.instrumenter.SourceInstrumenter
import kotlinx.coroutines.runBlocking

const val appName = "Edgar"
const val appVersion = "WIP-1.0"

internal val configuration by lazy { Settings.configuration }
internal val logger by lazy { Settings.logger }

fun main(args: Array<String>) {
    fun doCoverageWork() {
        runBlocking {
            SourceInstrumenter.doInstrumentation(appName, appVersion)
            DataCollector.doCoverageDataCollection()
            ReporterGenerator.doReportGeneration()
        }
    }

    require(args.isNotEmpty())
    configuration.validateArgs(args)
    doCoverageWork()
}
