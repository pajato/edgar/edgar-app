* Edgar: A Language Server Protocol Based Code Coverage Tool
** Description
Edgar is a code coverage/report generation tool that can be used to provide a code coverage report for a multi-language
project. It leverages the [[https://microsoft.github.io/language-server-protocol/][Language Server Protocol]] to instrument source code upon which data can be collected over
mulitple, independent runs on tests, sample code, benchmarks, etc. The goal is to provide the User (developer) with data
that identifies the project code not covered by a driving program (test, benchmark, etc.) A common use case is to run
all available tests using Edgar to identify all untested code.

Note that we make no claims about code quality or correctness. Rather, for a given suite of driving programs, Edgar
will generate a report that identifies the lines of code that were not executed. This may well be a surprise to a
developer and somehing that wants or needs to be corrected.

Edgar is also a suite of tools that instrument, collect data on and provide code coverage reports for any program or
library whose development language has an external server program/process which implements the Microsoft Language
Server Protocol extended to generate coverage data.
** The pieces
Edgar is a Kotlin program leveraging Kotlin coroutines that connect several interacting pieces.
*** The plugin wrappers
We envision Edgar to be wrapped in a plugin, much as Jacoco is wrapped in Maven, Ant or Gradle. A closure will specify
the particular configuration. The rest of this document will illustrate that closure using a Kotlin DSL script that will
be familiar to any developer who has ever seen an Android or IntelliJ Gradle plugin.
*** The main() function
main() must handle iniitialization of all coroutines.

How does it detect what language servers to start? Let's start with the Edgar configuration block:

edgar {
    servers = "kotlin-language-server", ...
}

Then these servers become main() arguments, i.e. each argument is a configuration item, consisting of a :
separated list of keywards using the form <type>:<param1>:<param2>:...<paramN> according to the following table:

| Type name | Param 1   | Param 2                  | Param N        |
|-----------+-----------+--------------------------+----------------|
| server    | command   | file extension           | file extension |
| logger    | directory | file name template (opt) |                |
| (tbd)     |           |                          |                |
|-----------+-----------+--------------------------+----------------|

For example, "server:kotlin-language-server:.kt:.kts". At least one server must be configured by the developer.  Failure
to find any will result in a fatal runtime error. The server name should be found on the search path at runtime
by the report generation task.
*** The Instrumentor
The instrumentor will use LSP to find all instances of executable source code for a program or library X and transform X
into an equivalent program or library X'.

Executable source code includes functions, constructors, and top-level variable declarations that include assigments.
*** The Coverage Data Collector
X', when run, will collect data on how many times executable code in X is invoked. The driving code and be tests, sample
runs, demos, or production runs. All are useful especially when automated such that they can be run from a CI tool such
as Team City, Jenkins, Travis CI, etc.
*** The Report Generator
The report generator will use any collected data to provide a report which reveals, most importantly, which code in X
has not been executed. This code should stand out as code that needs to be checked via some automated mechanism,
preferably a test but even a simple demo that does not test the code but does execute previously unexecuted code is
still very useful.
*** The LSP Extension
Each LSP instance must be enhanced (extended) by providing the instrumentation code which facilitates data collection.
** The Approach
Edgar will be a single executable taking form as, at first, a Gradle Plugin. All pieces of Edgar will be written in pure
Kotlin.

Edgar provides a core JSON-RPC mechanism that interacts with an LSP instance, the language server (LS). For those
familiar with Jacoco, a very popular JVM code coverage tool with a rich history, Edgar will use a similar Gradle
configuration closure that specifies the sources, inclusions, exclusions, report formats and locations.

For development language environments that do not support Gradle, alternative solutions need to be created. Cargo for
Rust, Xcode for Swift, something else for JavaScript, TypeScript, Dart, etc.

Edgar will be free software (GPL) with community contributions eagerly sought.
*** Unhappy Path Detection
Edgar will first ensure a suitable LS instance is available. Failing that causes a fatal error. Otherwise, Edgar
continues.
*** The Instrumentor
The LS will be initialized and all source files will be registered. The LS will then inform Edgar where all executable
statements can be found (source code locations). Edgar will copy the source files and add suitable instrumentation in
the copy.
*** The Collector
Edgar will then perform a complete build (rebuild) adding to any existing collected coverage data.
*** The Reporter
Edgar will then generate a report using the collected data.
** Medium Article
+ Unstated Goals: 4 minute read; readers will laugh, learn and share
+ Address myths of code coverage
+ State of the practice for common tools
+ Benefits of source transformation approach (see ACM article)
** References
+ [[http://www.semdesigns.com/Company/Publications/TestCoverage.pdf][Original article on src-src transformation]] (from 2002 via Ira Baxter, CTO, Semantic Designs, Inc Austin TX)
+ file this: http://ismail.badawi.io/blog/2013/05/03/writing-a-code-coverage-tool/
+ Anything on Jacoco, gcov etc.
** Random stuff
*** The Instrumentor
+ how it will interact with LSP: must leverage coroutines to support the JSON-RPC character of LSP. The LSP spec says
  that Edgar (the client) must initialize the LSP instance (the server). The client and server will honor an
  asynchronous request-response interaction with random client and/or server notifications interspersed throughout the
  life of both. Edgar will likely use a coroutine to generate requests and deliver them to the server. Edgar must
  also have a coroutine to collect replies and trigger subsequent handling.
+ This means I must build a mental model of how the coroutines play together.
*** Coroutines
+ teaching Kotlin coroutines will be required for contributors (with a few exceptions no doubt), to that end:
*** [[https://testing.googleblog.com/2020/08/code-coverage-best-practices.html][Google Testing Blog post]]
***** The Pieces of Coroutines
1. The Scope: the runtime object (CoroutineScope) that manages the work coroutines perform. Primarily handles
   canceling and clean up.
2. The Builder: functions which act as an umbrella over the work to be performed independently (the closure).
3. The Dispatcher: an aspect of the builder function (of type Dispacther) which specifies how to coordinate thread
   pooling.
4. The suspend Function: identifies code that can run in a builder or in another suspend marked function.
5. The Context: a CoroutineContext object that contains a Dispatcher and Job that are part and parcel of a
   coroutine.
6. The Job:
