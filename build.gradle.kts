plugins {
    kotlin("jvm") version "1.4.10"
    id("kotlinx-serialization") version "1.4.10"
    application
    jacoco
}

jacoco {
    toolVersion = "0.8.5"
    reportsDir = file("$buildDir/coverage")
}

tasks.test {
    finalizedBy(tasks.jacocoTestReport) // report is always generated after tests run
}

tasks.jacocoTestReport {
    dependsOn(tasks.test) // tests are required to run before generating the report
}

group = "com.pajato.edgar"
version = "0.9.1"

repositories {
    mavenCentral()
    jcenter()
    mavenLocal()
}

dependencies {
    // Align versions of all Kotlin components
    implementation(platform("org.jetbrains.kotlin:kotlin-bom"))

    implementation(kotlin("stdlib-jdk8"))
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:1.3.9")
    implementation("org.jetbrains.kotlinx:kotlinx-serialization-json:1.0.0")

    implementation("com.pajato.edgar:edgar-core:0.9.1")
    implementation("com.pajato.edgar:edgar-source-instrumenter:0.9.1")
    implementation("com.pajato.edgar:edgar-data-collector:1.0-SNAPSHOT")
    implementation("com.pajato.edgar:edgar-report-generator:1.0-SNAPSHOT")

    testImplementation("org.jetbrains.kotlin:kotlin-test")
    testImplementation("org.jetbrains.kotlin:kotlin-test-junit")
}

tasks {
    compileKotlin { kotlinOptions.jvmTarget = "1.8" }
    compileTestKotlin { kotlinOptions.jvmTarget = "1.8" }
}

application {
    mainClassName = "com.pajato.edgar.MainKt"
}
